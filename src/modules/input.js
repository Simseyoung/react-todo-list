/* Ducks 구조로 만드는 리덕스 Input 모듈 파일.
    1. 액션 타입 정의
    2. 액션 생성 함수 생성
    3. 리듀서 초기 상태 정의
    4. 리듀서 생성
*/

import { Map } from 'immutable';
import { handleActions, createAction } from 'redux-actions';

// 액션 타입
// 문자열의 앞부분에 리듀서의 이름을 적는다.
const SET_INPUT = 'input/SET_INPUT';

//액션 함수
export const setInput = createAction(SET_INPUT);

// 리듀서 초기 상태
const initialState = Map({
  value: '',
});

// 리듀서 생성
export default handleActions(
  {
    [SET_INPUT]: (state, action) => {
      return state.set('value', action.payload);
    },
  },
  initialState
);
