import React from 'react';
import styles from './TodoInput.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

//input과 버튼이 함께 있는 컴포넌트입니다.
/*  value: input 값
    onChange: input 변경 이벤트
    onInsert: 추가 버튼 클릭 이벤트
*/

// class가 아니라 함수형 컴포넌트인 이유는, 상태 값을 저장할 게 없고, 변경되는게 없기 때문이다.

const TodoInput = ({ value, onChange, onInsert }) => {
  //Enter 키가 눌리면 onInsert를 실행합니다.
  const handleKeyPress = e => {
    if (e.key === 'Enter') {
      onInsert();
    }
  };

  return (
    <div className={cx('todo-input')}>
      <input onChange={onChange} value={value} onKeyPress={handleKeyPress} />
      <div className={cx('add-button')} onClick={onInsert}>
        추가
      </div>
    </div>
  );
};

export default TodoInput;
